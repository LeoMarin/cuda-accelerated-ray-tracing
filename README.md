## GPU Accelerated Ray Tracing using CUDA

GPU Accelerated version of [CPU Ray Tracing](https://gitlab.com/LeoMarin/cpu-ray-tracing) using CUDA.