#pragma once

#include "DeviceVector.h"
#include "shape.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

struct Vertex
{
	int positionIndex;
	int uvIndex;
	int normalIndex;
};

struct Face
{
	Vertex vertices[3];
};

class MeshVertexData
{
public:
	__host__ MeshVertexData(const char* fileName);

public:
	// Mesh Public Data
	DeviceVector<Face> faces;
	DeviceVector<Vector3f> positions;
	DeviceVector<Vector3f> normals;
	DeviceVector<Vector3f> uvs;
};

__host__ inline MeshVertexData::MeshVertexData(const char* fileName)
	: faces(), positions(), normals(), uvs()
{
	// open .obj file
	std::ifstream objFile(fileName);
	if (!objFile.is_open())
	{
		std::cout << "OBJ file not opened correctly" << std::endl;
		return;
	}

	// read .obj line by line
	std::string line;
	while (!objFile.eof())
	{
		// read line
		std::getline(objFile, line);
		std::istringstream iss(line.c_str());

		// get prefix
		std::string prefix;
		iss >> prefix;

		// read vertex
		if (prefix == "v")
		{
			Vector3f temp;
			iss >> temp.x >> temp.y >> temp.z;
			positions.PushBack(temp);
		}
		// read texture coordinates
		else if (prefix == "vt")
		{
			Vector3f temp;
			iss >> temp.x >> temp.y;
			uvs.PushBack(temp);
		}
		// read normal
		else if (prefix == "vn")
		{
			Vector3f temp;
			iss >> temp.x >> temp.y >> temp.z;
			normals.PushBack(temp);
		}
		// read face
		else if (prefix == "f")
		{
			// ever face is described by 3 vertices
			// vertices consist of position, uv and normal
			// each line looks like: *** f pos/uv/norm pos/uv/norm pos/uv/norm ***

			Vertex vertex;
			Face face;
			char trash;

			// read 3 vertices in face
			for (int i = 0; i < 3; i++)
			{
				iss >> vertex.positionIndex;
				iss >> trash;
				iss >> vertex.uvIndex;
				iss >> trash;
				iss >> vertex.normalIndex;

				// in wavefront obj all indices start at 1, not 0
				vertex.positionIndex--;
				vertex.uvIndex--;
				vertex.normalIndex--;

				face.vertices[i] = vertex;
			}

			// store current face in m_Faces
			faces.PushBack(face);
		}
	}
}

// Model Definition
class TriangleMesh
{
public:
	// Model Public Methods
	__device__ TriangleMesh(Transform transform, MeshVertexData* mesh, Material* material, DeviceVector<Shape*>& objects);

	__device__ inline Vector3f VertexPosition(int ithFace, int nthVertex) const;
	__device__ inline Vector3f VertexNormal(int ithFace, int nthVertex) const;
	__device__ inline Vector3f VertexUV(int ithFace, int nthVertex) const;

	__device__ inline size_t NumberOfVertices() const;
	__device__ inline size_t NumberOfFaces() const;

	// each face looks like: [ {posI, uvI, normI}, {posI, uvI, normI}, {posI, uvI, normI} ]
	DeviceVector<Face> faces;

	Material* material;
private:
	// Model Private Variables
	DeviceVector<Vector3f> positions;
	DeviceVector<Vector3f> normals;
	DeviceVector<Vector3f> uvs;
};

class Triangle : public Shape
{
public:
	// MeshTriangle Public Methods
	__device__ Triangle(Transform transform, TriangleMesh* mesh, int faceIndex) 
		: Shape(transform), mesh(mesh), faceIndex(faceIndex) {}

	__device__ ~Triangle() {}

	// Return true if ray intersects triangle
	__device__ bool Intersect(const Ray& ray, SurfaceInteraction* isect) const override;

	// Returns AABB of triangle
	//Bounds3f BoundingBox() const;
private:
	// MeshTriangle Private Data
	TriangleMesh* mesh;
	int faceIndex;
};

__device__ TriangleMesh::TriangleMesh(Transform transform, MeshVertexData* mesh, Material* material, DeviceVector<Shape*>& objects)
	:material(material), faces()
{
	for (int i = 0; i < mesh->faces.Size(); i++)
	{
	}

	//for (size_t i = 0; i < faces.Size(); i++)
	//{
	//	objects.PushBack(new Triangle(transform, this, i));
	//}
}


__device__ inline size_t TriangleMesh::NumberOfVertices() const
{
	return positions.Size();
}

__device__ inline size_t TriangleMesh::NumberOfFaces() const
{
	return faces.Size();
}

__device__ inline Vector3f TriangleMesh::VertexPosition(int ithFace, int nthVertex) const
{	
	int i = faces[ithFace].vertices[nthVertex].positionIndex;
	return positions[i];
}

__device__ inline Vector3f TriangleMesh::VertexNormal(int ithFace, int nthVertex) const
{
	int i = faces[ithFace].vertices[nthVertex].normalIndex;
	return normals[i];
}

__device__ inline Vector3f TriangleMesh::VertexUV(int ithFace, int nthVertex) const
{
	int i = faces[ithFace].vertices[nthVertex].uvIndex;
	return uvs[i];
}

__device__ inline bool Triangle::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);

	// Moller-Trumbore ray/triangle intersection

	// Rename vertices for easy access
	Vector3f a = mesh->VertexPosition(faceIndex, 0);
	Vector3f b = mesh->VertexPosition(faceIndex, 1);
	Vector3f c = mesh->VertexPosition(faceIndex, 2);

	// Find two edges of a triangle
	Vector3f edge1 = b - a;
	Vector3f edge2 = c - a;

	// calculate determinante
	Vector3f pVec = Cross(ray.direction, edge2);
	float det = Dot(edge1, pVec);

	// if determinante is near zero, the ray does not intersect triangle plane
	if (det > -ShadowEpsilon && det < ShadowEpsilon)
		return false;

	// used to avoid floating point division
	float invDet = 1 / det;

	// distance from vertex to ray origin
	Vector3f tVec = Vector3f(ray.origin) - a;

	// u and v barycentric coordinates
	// calculate u parameter and test bounds
	float u = Dot(tVec, pVec) * invDet;
	if (u < 0 || u > 1)
		return false;

	// calculate v parameter and test bounds
	Vector3f qVec = Cross(tVec, edge1);
	float v = Dot(ray.direction, qVec) * invDet;
	if (v < 0 || u + v > 1)
		return false;

	// calculate ray t parameter for intersection
	float t = Dot(edge2, qVec) * invDet;

	// if this is the closest object hit, store relevant values
	if (t < ray.tMax && t > ShadowEpsilon)
	{
		r.tMax = t;
		isect->point = ObjectToWorld(ray(t));
		// calculate normal in current point
		isect->normal = ObjectToWorld(Normal3f(
			(1 - u - v) * mesh->VertexNormal(faceIndex, 0) +
			u * mesh->VertexNormal(faceIndex, 1) +
			v * mesh->VertexNormal(faceIndex, 2)));
		isect->material = mesh->material;
		return true;
	}

	return false;
}

//Bounds3f Triangle::BoundingBox() const
//{
//	// Rename vertices for easy access
//	Vec3f& a = mesh->VertexPosition(faceIndex, 0);
//	Vec3f& b = mesh->VertexPosition(faceIndex, 1);
//	Vec3f& c = mesh->VertexPosition(faceIndex, 2);
//	return Union(Bounds3f(a, b), c);
//}