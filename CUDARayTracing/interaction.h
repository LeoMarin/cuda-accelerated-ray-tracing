#pragma once

#include "geometry.h"

class Material;

// Interaction Declarations
class Interaction
{
public:
	// Interaction Public Methods
	__device__ Interaction() {}
	__device__ Interaction(const Point3f& point, const Normal3f& normal)
		:point(point), normal(normal)
	{}
	__device__ ~Interaction() {}

	// Interaction Public Data
	Point3f point;
	Normal3f normal;
};

// SurfaceInteraction Declarations
class SurfaceInteraction : public Interaction
{
public:
	// SurfaceInteraction Public Methods
	__device__ SurfaceInteraction() : material(nullptr) {}
	__device__ SurfaceInteraction(const Point3f& point, const Normal3f& normal, Material* material)
		:Interaction(point, normal), material(material)
	{}
	__device__ ~SurfaceInteraction() {}

	Material* material;
};