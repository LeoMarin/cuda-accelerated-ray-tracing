#pragma once

#include "shape.h"

class Plane : public Shape
{
public:
	// Plane Public Methods
	__device__ Plane() : material(nullptr) {}
	__device__ Plane(Transform transform, Material* material) : Shape(transform), material(material) {}

	__device__ ~Plane() { delete material; }
	// Return true if ray intersects plane
	__device__ bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
	// Calculate plane normal in point
	__device__ inline Normal3f NormalInPoint(const Point3f& point) const;
	// Returns AABB of plane
	//Bounds3f BoundingBox() const;
private:
	// Plane Private Data
	Material* material;
};

__device__ inline bool Plane::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);

	if (std::abs(ray.direction.y) < ShadowEpsilon)
		return false;

	float t = -ray.origin.y / ray.direction.y;
	// check hit
	if (t > ShadowEpsilon && r.tMax > t)
	{
		r.tMax = t;
		isect->point = ObjectToWorld(ray(t));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t)));
		isect->material = material;
		return true;
	}

	return false;
}

__device__ inline Normal3f Plane::NormalInPoint(const Point3f& point) const
{
	// v = p - center
	// vector v has direction of sphere normal in point p
	return Normal3f(0, 1, 0);
}