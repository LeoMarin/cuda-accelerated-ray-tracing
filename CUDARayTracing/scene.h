#pragma once

#include "DeviceVector.h"
#include "shape.h"
#include "plane.h"
#include "cylinder.h"
#include "cone.h"
#include "cube.h"
#include "triangle.h"

// Scene Declaration
class Scene
{
public:
    __device__ Scene(MeshVertexData* mesh);
    __device__ ~Scene() {}
    __device__ bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;

private:
    DeviceVector<Shape*> shapes;
};

__device__ Scene::Scene(MeshVertexData* mesh)
{
    //Transform T1;
    //shapes.PushBack(new Sphere(1, T1, new Diffuse(Vector3f(0.8f))));

    //Transform T2 = Translate(Vector3f(0, -101, 0));
    //shapes.PushBack(new Sphere(100, T2, new Diffuse(Vector3f(0.5f))));

    Transform T3 = Translate(Vector3f(0, -1, 0));
    shapes.PushBack(new Plane(T3, new Diffuse(Vector3f(0.3f))));

    //Transform T4 = Translate(Vector3f(5, 0, 5)) * RotateY(0) * Scale(0.1f, 1, 1);
    //shapes.PushBack(new Cube(T4, new Metalic(Vector3f(0.0f, 0.6f, 0.3f), 0.7f)));

    //Transform T5 = Translate(Vector3f(0, 0, 0));
    //shapes.PushBack(new Cube(T5, new Metalic(Vector3f(0.5f, 0.2f, 0.5f), 0.5f)));

    //Transform T6 = Translate(Vector3f(-4, -0.9f, 5)) * Scale(1, 0.1f, 0.6f);
    //shapes.PushBack(new Cube(T6, new Diffuse(Vector3f(0.6f, 0.3f, 0.2f))));

    //Transform T7 = Translate(Vector3f(6, 0, -8));
    //shapes.PushBack(new Cube(T7, new Diffuse(Vector3f(0.15f))));

    //Transform T8 = Translate(Vector3f(-3, -0.5f, 1)) * Scale(1, 0.5f, 0.2f) * RotateY(65);
    //shapes.PushBack(new Cube(T8, new Diffuse(Vector3f(0.2f, 0.6f, 0.4f))));

    //Transform T9 = Translate(Vector3f(7, -0.8f, -15)) * RotateY(-50) * Scale(0.2f, 0.2f, 20);
    //shapes.PushBack(new Cube(T9, new Metalic(Vector3f(0.9f), 0.6f)));

    //Transform T10 = Translate(Vector3f(-10, 0, -18)) * Scale(1, 1, 1);;
    //shapes.PushBack(new Cube(T10, new Diffuse(Vector3f(0.5f, 0.2f, 0.2f))));

    //Transform T11 = Translate(Vector3f(-7, 0, -18)) * Scale(1, 1.8f, 1);;
    //shapes.PushBack(new Cube(T11, new Diffuse(Vector3f(0.5f, 0.5f, 0.2f))));

    //Transform T12 = Translate(Vector3f(-4, 0, -18)) * Scale(1, 2.6f, 1);;
    //shapes.PushBack(new Cube(T12, new Diffuse(Vector3f(0.2f, 0.5f, 0.2f))));

    //Transform T13 = Translate(Vector3f(-1, 0, -18)) * Scale(1, 3.4f, 1);;
    //shapes.PushBack(new Cube(T13, new Diffuse(Vector3f(0.2f, 0.5f, 0.5f))));

    //Transform T14 = Translate(Vector3f(2, 0, -18)) * Scale(1, 4.2f, 1);
    //shapes.PushBack(new Cube(T14, new Diffuse(Vector3f(0.2f, 0.2f, 0.5f))));

    //Transform T15 = Translate(Vector3f(0, 0, 0)) * RotateY(45);
    //shapes.PushBack(new Cube(T15, new Diffuse(Vector3f(0.2f, 0.2f, 0.5f))));

    Transform T7;
    TriangleMesh* triangleMesh = new TriangleMesh(T7, mesh, new Diffuse({ 0.4f, 0.4f, 0.8f }), shapes);
}

// Scene Method Definitions
__device__ bool Scene::Intersect(const Ray& ray, SurfaceInteraction* isect) const
{
    bool hit = false;

    // loop over every object in the scene
    for (int i = 0; i < shapes.Size(); i++)
    {
        // intersect every object with ray
        if (shapes[i]->Intersect(ray, isect))
            hit = true;
    }
    

    return hit;
}
