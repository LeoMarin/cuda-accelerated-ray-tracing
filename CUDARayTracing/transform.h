#pragma once

#include <cmath>
#include <iostream>

#include "geometry.h"

/*
/
/ *** Matrix4x4 ***
/
*/

struct Matrix4x4
{
    // Matrix public methods
    __host__ __device__ Matrix4x4();
    __host__ __device__ Matrix4x4(float mat[4][4]);
    __host__ __device__ Matrix4x4(float t00, float t01, float t02, float t03, float t10, float t11,
        float t12, float t13, float t20, float t21, float t22, float t23,
        float t30, float t31, float t32, float t33);
    __host__ __device__ bool operator==(const Matrix4x4& m2) const;
    __host__ __device__ bool operator!=(const Matrix4x4& m2) const;

    __host__ __device__ friend Matrix4x4 Transpose(const Matrix4x4& m);
    __host__ __device__ static Matrix4x4 Mul(const Matrix4x4& m1, const Matrix4x4& m2);
    __host__ __device__ friend Matrix4x4 Inverse(const Matrix4x4& m);

    // Matrix public data
    float m[4][4];
};

/*
/
/ *** Transofrm ***
/
*/

class Transform
{
public:
    // Transform public methods
    __host__ __device__ Transform() {}    
    __host__ __device__ Transform(const float mat[4][4]) {
        m = Matrix4x4(mat[0][0], mat[0][1], mat[0][2], mat[0][3], mat[1][0],
            mat[1][1], mat[1][2], mat[1][3], mat[2][0], mat[2][1],
            mat[2][2], mat[2][3], mat[3][0], mat[3][1], mat[3][2],
            mat[3][3]);
        mInv = Inverse(m);
    }
    __host__ __device__ Transform(const Matrix4x4 &m) : m(m), mInv(Inverse(m)) {}
    __host__ __device__ Transform(const Matrix4x4 &m, const Matrix4x4 &mInv) : m(m), mInv(mInv) {}

    __host__ __device__ friend Transform Inverse(const Transform& t) { return Transform(t.mInv, t.m); }
    __host__ __device__ friend Transform Transpose(const Transform& t) { return Transform(Transpose(t.m), Transpose(t.mInv)); }
    __host__ __device__ bool operator==(const Transform& t) const { return t.m == m && t.mInv == mInv; }
    __host__ __device__ bool operator!=(const Transform& t) const { return t.m != m || t.mInv != mInv; }

    __host__ __device__ bool operator<(const Transform& t2) const;
    __host__ __device__ bool IsIdentity() const;

    __host__ __device__ const Matrix4x4 &GetMatrix() const { return m; }
    __host__ __device__ const Matrix4x4 &GetInverseMatrix() const { return mInv; }

    __host__ __device__ bool SwapsHandedness() const;

    __host__ __device__ Transform operator*(const Transform& t2) const;

    __device__ inline Point3f operator()(const Point3f& p) const;
    __device__ inline Vector3f operator()(const Vector3f& v) const;
    __device__ inline Normal3f operator()(const Normal3f&) const;
    __device__ inline Ray operator()(const Ray& r) const;
    // SurfaceInteraction operator()(const SurfaceInteraction &si) const;
    __device__ inline Point3f operator()(const Point3f& pt, Vector3f* absError) const;
    __device__ inline Point3f operator()(const Point3f& p, const Vector3f& pError, Vector3f* pTransError) const;
    __device__ inline Vector3f operator()(const Vector3f& v, Vector3f* vTransError) const;
    __device__ inline Vector3f operator()(const Vector3f& v, const Vector3f& vError, Vector3f* vTransError) const;
    __device__ inline Ray operator()(const Ray& r, Vector3f* oError, Vector3f* dError) const;
    __device__ inline Ray operator()(const Ray& r, const Vector3f& oErrorIn, const Vector3f& dErrorIn, Vector3f* oErrorOut, Vector3f* dErrorOut) const;

private:
    // Transform private data
    Matrix4x4 m, mInv;
};

__host__ __device__ Transform Translate(const Vector3f& delta);
__host__ __device__ Transform Scale(float x, float y, float z);
__host__ __device__ Transform RotateX(float theta);
__host__ __device__ Transform RotateY(float theta);
__host__ __device__ Transform RotateZ(float theta);
__host__ __device__ Transform Rotate(float theta, const Vector3f& axis);
__host__ __device__ Transform LookAt(const Point3f& pos, const Point3f& look, const Vector3f& up);
__host__ __device__ Transform Orthographic(float znear, float zfar);
__host__ __device__ Transform Perspective(float fov, float znear, float zfar);
__host__ __device__ bool SolveLinearSystem2x2(const float A[2][2], const float B[2], float* x0, float* x1);


// Transform Inline Functions
__device__ inline Point3f Transform::operator()(const Point3f& p) const
{
    float x = p.x, y = p.y, z = p.z;
    float xp = m.m[0][0] * x + m.m[0][1] * y + m.m[0][2] * z + m.m[0][3];
    float yp = m.m[1][0] * x + m.m[1][1] * y + m.m[1][2] * z + m.m[1][3];
    float zp = m.m[2][0] * x + m.m[2][1] * y + m.m[2][2] * z + m.m[2][3];
    float wp = m.m[3][0] * x + m.m[3][1] * y + m.m[3][2] * z + m.m[3][3];
    if (wp == 1)
        return Point3f(xp, yp, zp);
    else
        return Point3f(xp, yp, zp) / wp;
}

__device__ inline Vector3f Transform::operator()(const Vector3f& v) const
{
    float x = v.x, y = v.y, z = v.z;
    return Vector3f(m.m[0][0] * x + m.m[0][1] * y + m.m[0][2] * z,
        m.m[1][0] * x + m.m[1][1] * y + m.m[1][2] * z,
        m.m[2][0] * x + m.m[2][1] * y + m.m[2][2] * z);
}

__device__ inline Normal3f Transform::operator()(const Normal3f& n) const
{
    float x = n.x, y = n.y, z = n.z;
    return Normal3f(mInv.m[0][0] * x + mInv.m[1][0] * y + mInv.m[2][0] * z,
        mInv.m[0][1] * x + mInv.m[1][1] * y + mInv.m[2][1] * z,
        mInv.m[0][2] * x + mInv.m[1][2] * y + mInv.m[2][2] * z);
}

__device__ inline Ray Transform::operator()(const Ray& r) const
{
    Vector3f oError;
    Point3f o = (*this)(r.origin, &oError);
    Vector3f d = (*this)(r.direction);
    // Offset ray origin to edge of error bounds and compute _tMax_
    float lengthSquared = d.LengthSquared();
    float tMax = r.tMax;
    if (lengthSquared > 0) {
        float dt = Dot(Abs(d), oError) / lengthSquared;
        o += d * dt;
        tMax -= dt;
    }
    return Ray(o, d, tMax);
}

__device__ inline Point3f Transform::operator()(const Point3f& p, Vector3f* pError) const
{
    float x = p.x, y = p.y, z = p.z;
    // Compute transformed coordinates from point _pt_
    float xp = (m.m[0][0] * x + m.m[0][1] * y) + (m.m[0][2] * z + m.m[0][3]);
    float yp = (m.m[1][0] * x + m.m[1][1] * y) + (m.m[1][2] * z + m.m[1][3]);
    float zp = (m.m[2][0] * x + m.m[2][1] * y) + (m.m[2][2] * z + m.m[2][3]);
    float wp = (m.m[3][0] * x + m.m[3][1] * y) + (m.m[3][2] * z + m.m[3][3]);

    // Compute absolute error for transformed point
    float xAbsSum = (std::abs(m.m[0][0] * x) + std::abs(m.m[0][1] * y) +
        std::abs(m.m[0][2] * z) + std::abs(m.m[0][3]));
    float yAbsSum = (std::abs(m.m[1][0] * x) + std::abs(m.m[1][1] * y) +
        std::abs(m.m[1][2] * z) + std::abs(m.m[1][3]));
    float zAbsSum = (std::abs(m.m[2][0] * x) + std::abs(m.m[2][1] * y) +
        std::abs(m.m[2][2] * z) + std::abs(m.m[2][3]));
    *pError = gamma(3) * Vector3f(xAbsSum, yAbsSum, zAbsSum);
    if (wp == 1)
        return Point3f(xp, yp, zp);
    else
        return Point3f(xp, yp, zp) / wp;
}

__device__ inline Point3f Transform::operator()(const Point3f& pt, const Vector3f& ptError, Vector3f* absError) const
{
    float x = pt.x, y = pt.y, z = pt.z;
    float xp = (m.m[0][0] * x + m.m[0][1] * y) + (m.m[0][2] * z + m.m[0][3]);
    float yp = (m.m[1][0] * x + m.m[1][1] * y) + (m.m[1][2] * z + m.m[1][3]);
    float zp = (m.m[2][0] * x + m.m[2][1] * y) + (m.m[2][2] * z + m.m[2][3]);
    float wp = (m.m[3][0] * x + m.m[3][1] * y) + (m.m[3][2] * z + m.m[3][3]);
    absError->x =
        (gamma(3) + (float)1) *
        (std::abs(m.m[0][0]) * ptError.x + std::abs(m.m[0][1]) * ptError.y +
            std::abs(m.m[0][2]) * ptError.z) +
        gamma(3) * (std::abs(m.m[0][0] * x) + std::abs(m.m[0][1] * y) +
            std::abs(m.m[0][2] * z) + std::abs(m.m[0][3]));
    absError->y =
        (gamma(3) + (float)1) *
        (std::abs(m.m[1][0]) * ptError.x + std::abs(m.m[1][1]) * ptError.y +
            std::abs(m.m[1][2]) * ptError.z) +
        gamma(3) * (std::abs(m.m[1][0] * x) + std::abs(m.m[1][1] * y) +
            std::abs(m.m[1][2] * z) + std::abs(m.m[1][3]));
    absError->z =
        (gamma(3) + (float)1) *
        (std::abs(m.m[2][0]) * ptError.x + std::abs(m.m[2][1]) * ptError.y +
            std::abs(m.m[2][2]) * ptError.z) +
        gamma(3) * (std::abs(m.m[2][0] * x) + std::abs(m.m[2][1] * y) +
            std::abs(m.m[2][2] * z) + std::abs(m.m[2][3]));
    if (wp == 1.)
        return Point3f(xp, yp, zp);
    else
        return Point3f(xp, yp, zp) / wp;
}

__device__ inline Vector3f Transform::operator()(const Vector3f& v, Vector3f* absError) const
{
    float x = v.x, y = v.y, z = v.z;
    absError->x =
        gamma(3) * (std::abs(m.m[0][0] * v.x) + std::abs(m.m[0][1] * v.y) +
            std::abs(m.m[0][2] * v.z));
    absError->y =
        gamma(3) * (std::abs(m.m[1][0] * v.x) + std::abs(m.m[1][1] * v.y) +
            std::abs(m.m[1][2] * v.z));
    absError->z =
        gamma(3) * (std::abs(m.m[2][0] * v.x) + std::abs(m.m[2][1] * v.y) +
            std::abs(m.m[2][2] * v.z));
    return Vector3f(m.m[0][0] * x + m.m[0][1] * y + m.m[0][2] * z,
        m.m[1][0] * x + m.m[1][1] * y + m.m[1][2] * z,
        m.m[2][0] * x + m.m[2][1] * y + m.m[2][2] * z);
}

__device__ inline Vector3f Transform::operator()(const Vector3f& v, const Vector3f& vError, Vector3f* absError) const
{
    float x = v.x, y = v.y, z = v.z;
    absError->x =
        (gamma(3) + (float)1) *
        (std::abs(m.m[0][0]) * vError.x + std::abs(m.m[0][1]) * vError.y +
            std::abs(m.m[0][2]) * vError.z) +
        gamma(3) * (std::abs(m.m[0][0] * v.x) + std::abs(m.m[0][1] * v.y) +
            std::abs(m.m[0][2] * v.z));
    absError->y =
        (gamma(3) + (float)1) *
        (std::abs(m.m[1][0]) * vError.x + std::abs(m.m[1][1]) * vError.y +
            std::abs(m.m[1][2]) * vError.z) +
        gamma(3) * (std::abs(m.m[1][0] * v.x) + std::abs(m.m[1][1] * v.y) +
            std::abs(m.m[1][2] * v.z));
    absError->z =
        (gamma(3) + (float)1) *
        (std::abs(m.m[2][0]) * vError.x + std::abs(m.m[2][1]) * vError.y +
            std::abs(m.m[2][2]) * vError.z) +
        gamma(3) * (std::abs(m.m[2][0] * v.x) + std::abs(m.m[2][1] * v.y) +
            std::abs(m.m[2][2] * v.z));
    return Vector3f(m.m[0][0] * x + m.m[0][1] * y + m.m[0][2] * z,
        m.m[1][0] * x + m.m[1][1] * y + m.m[1][2] * z,
        m.m[2][0] * x + m.m[2][1] * y + m.m[2][2] * z);
}

__device__ inline Ray Transform::operator()(const Ray& r, Vector3f* oError, Vector3f* dError) const
{
    Point3f o = (*this)(r.origin, oError);
    Vector3f d = (*this)(r.direction, dError);
    float tMax = r.tMax;
    float lengthSquared = d.LengthSquared();
    if (lengthSquared > 0) {
        float dt = Dot(Abs(d), *oError) / lengthSquared;
        o += d * dt;
        //        tMax -= dt;
    }
    return Ray(o, d, tMax);
}

__device__ inline Ray Transform::operator()(const Ray& r, const Vector3f& oErrorIn, const Vector3f& dErrorIn, Vector3f* oErrorOut, Vector3f* dErrorOut) const
{
    Point3f o = (*this)(r.origin, oErrorIn, oErrorOut);
    Vector3f d = (*this)(r.direction, dErrorIn, dErrorOut);
    float tMax = r.tMax;
    float lengthSquared = d.LengthSquared();
    if (lengthSquared > 0) {
        float dt = Dot(Abs(d), *oErrorOut) / lengthSquared;
        o += d * dt;
        //        tMax -= dt;
    }
    return Ray(o, d, tMax);
}

/*
/
/ *** Matrix4x4 ***
/
*/

__host__ __device__ Matrix4x4::Matrix4x4()
{
    m[0][0] = m[1][1] = m[2][2] = m[3][3] = 1.f;
    m[0][1] = m[0][2] = m[0][3] = m[1][0] = m[1][2] = m[1][3] = m[2][0] =
        m[2][1] = m[2][3] = m[3][0] = m[3][1] = m[3][2] = 0.f;
}

__host__ __device__ Matrix4x4::Matrix4x4(float mat[4][4])
{
    memcpy(m, mat, 16 * sizeof(float));
}

__host__ __device__ Matrix4x4::Matrix4x4(float t00, float t01, float t02, float t03, float t10, float t11, float t12, float t13, float t20, float t21, float t22, float t23, float t30, float t31, float t32, float t33)
{
    m[0][0] = t00; m[0][1] = t01; m[0][2] = t02; m[0][3] = t03;
    m[1][0] = t10; m[1][1] = t11; m[1][2] = t12; m[1][3] = t13;
    m[2][0] = t20; m[2][1] = t21; m[2][2] = t22; m[2][3] = t23;
    m[3][0] = t30; m[3][1] = t31; m[3][2] = t32; m[3][3] = t33;
}

__host__ __device__ inline bool Matrix4x4::operator==(const Matrix4x4& m2) const
{
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            if (m[i][j] != m2.m[i][j]) return false;
    return true;
}

__host__ __device__ inline bool Matrix4x4::operator!=(const Matrix4x4& m2) const
{
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            if (m[i][j] != m2.m[i][j]) return true;
    return false;
}

__host__ __device__ Matrix4x4 Transpose(const Matrix4x4& m)
{
    return Matrix4x4(m.m[0][0], m.m[1][0], m.m[2][0], m.m[3][0], m.m[0][1],
        m.m[1][1], m.m[2][1], m.m[3][1], m.m[0][2], m.m[1][2],
        m.m[2][2], m.m[3][2], m.m[0][3], m.m[1][3], m.m[2][3],
        m.m[3][3]);
}

__host__ __device__ Matrix4x4 Matrix4x4::Mul(const Matrix4x4& m1, const Matrix4x4& m2)
{
    Matrix4x4 r;
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            r.m[i][j] = m1.m[i][0] * m2.m[0][j] + m1.m[i][1] * m2.m[1][j] +
            m1.m[i][2] * m2.m[2][j] + m1.m[i][3] * m2.m[3][j];
    return r;
}

__host__ __device__ Matrix4x4 Inverse(const Matrix4x4& m)
{
    int indxc[4], indxr[4];
    int ipiv[4] = {0, 0, 0, 0};
    float minv[4][4];
    memcpy(minv, m.m, 4 * 4 * sizeof(float));
    for (int i = 0; i < 4; i++) {
        int irow = 0, icol = 0;
        float big = 0.f;
        // Choose pivot
        for (int j = 0; j < 4; j++) {
            if (ipiv[j] != 1) {
                for (int k = 0; k < 4; k++) {
                    if (ipiv[k] == 0) {
                        if (std::abs(minv[j][k]) >= big) {
                            big = float(std::abs(minv[j][k]));
                            irow = j;
                            icol = k;
                        }
                    }
                    else if (ipiv[k] > 1)
                        Matrix4x4();
                }
            }
        }
        ++ipiv[icol];
        // Swap rows _irow_ and _icol_ for pivot
        if (irow != icol) {
            for (int k = 0; k < 4; ++k) swap(minv[irow][k], minv[icol][k]);            
        }
        indxr[i] = irow;
        indxc[i] = icol;
        if (minv[icol][icol] == 0.f) Matrix4x4();

        // Set $m[icol][icol]$ to one by scaling row _icol_ appropriately
        float pivinv = 1.f / minv[icol][icol];
        minv[icol][icol] = 1.;
        for (int j = 0; j < 4; j++) minv[icol][j] *= pivinv;

        // Subtract this row from others to zero out their columns
        for (int j = 0; j < 4; j++) {
            if (j != icol) {
                float save = minv[j][icol];
                minv[j][icol] = 0;
                for (int k = 0; k < 4; k++) minv[j][k] -= minv[icol][k] * save;
            }
        }
    }
    // Swap columns to reflect permutation
    for (int j = 3; j >= 0; j--) {
        if (indxr[j] != indxc[j]) {
            for (int k = 0; k < 4; k++)
                swap(minv[k][indxr[j]], minv[k][indxc[j]]);
        }
    }
    return Matrix4x4(minv);
}

/*
/
/ *** Transofrm ***
/
*/

__host__ __device__ inline bool Transform::operator<(const Transform& t2) const
{
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j) {
            if (m.m[i][j] < t2.m.m[i][j]) return true;
            if (m.m[i][j] > t2.m.m[i][j]) return false;
        }
    return false;
}

__host__ __device__ inline bool Transform::IsIdentity() const
{
    return (m.m[0][0] == 1.f && m.m[0][1] == 0.f && m.m[0][2] == 0.f &&
        m.m[0][3] == 0.f && m.m[1][0] == 0.f && m.m[1][1] == 1.f &&
        m.m[1][2] == 0.f && m.m[1][3] == 0.f && m.m[2][0] == 0.f &&
        m.m[2][1] == 0.f && m.m[2][2] == 1.f && m.m[2][3] == 0.f &&
        m.m[3][0] == 0.f && m.m[3][1] == 0.f && m.m[3][2] == 0.f &&
        m.m[3][3] == 1.f);
}

__host__ __device__ bool SolveLinearSystem2x2(const float A[2][2], const float B[2], float* x0, float* x1)
{
    float det = A[0][0] * A[1][1] - A[0][1] * A[1][0];
    if (std::abs(det) < 1e-10f) return false;
    *x0 = (A[1][1] * B[0] - A[0][1] * B[1]) / det;
    *x1 = (A[0][0] * B[1] - A[1][0] * B[0]) / det;
    return true;
}

__host__ __device__ Transform Translate(const Vector3f& delta)
{
    Matrix4x4 m(1, 0, 0, delta.x, 0, 1, 0, delta.y, 0, 0, 1, delta.z, 0, 0, 0, 1);
    Matrix4x4 minv(1, 0, 0, -delta.x, 0, 1, 0, -delta.y, 0, 0, 1, -delta.z, 0, 0, 0, 1);
    return Transform(m, minv);
}

__host__ __device__ Transform Scale(float x, float y, float z)
{
    Matrix4x4 m(x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1);
    Matrix4x4 minv(1 / x, 0, 0, 0, 0, 1 / y, 0, 0, 0, 0, 1 / z, 0, 0, 0, 0, 1);
    return Transform(m, minv);
}

__host__ __device__ Transform RotateX(float theta) 
{
    float sinTheta = std::sin(Radians(theta));
    float cosTheta = std::cos(Radians(theta));
    Matrix4x4 m(1, 0, 0, 0, 0, cosTheta, -sinTheta, 0, 0, sinTheta, cosTheta, 0, 0, 0, 0, 1);
    return Transform(m, Transpose(m));
}

__host__ __device__ Transform RotateY(float theta) 
{
    float sinTheta = std::sin(Radians(theta));
    float cosTheta = std::cos(Radians(theta));
    Matrix4x4 m(cosTheta, 0, sinTheta, 0, 0, 1, 0, 0, -sinTheta, 0, cosTheta, 0, 0, 0, 0, 1);
    return Transform(m, Transpose(m));
}

__host__ __device__ Transform RotateZ(float theta) 
{
    float sinTheta = std::sin(Radians(theta));
    float cosTheta = std::cos(Radians(theta));
    Matrix4x4 m(cosTheta, -sinTheta, 0, 0, sinTheta, cosTheta, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    return Transform(m, Transpose(m));
}

__host__ __device__ Transform Rotate(float theta, const Vector3f& axis)
{
    Vector3f a = Normalize(axis);
    float sinTheta = std::sin(Radians(theta));
    float cosTheta = std::cos(Radians(theta));
    Matrix4x4 m;
    // Compute rotation of first basis vector
    m.m[0][0] = a.x * a.x + (1 - a.x * a.x) * cosTheta;
    m.m[0][1] = a.x * a.y * (1 - cosTheta) - a.z * sinTheta;
    m.m[0][2] = a.x * a.z * (1 - cosTheta) + a.y * sinTheta;
    m.m[0][3] = 0;

    // Compute rotations of second and third basis vectors
    m.m[1][0] = a.x * a.y * (1 - cosTheta) + a.z * sinTheta;
    m.m[1][1] = a.y * a.y + (1 - a.y * a.y) * cosTheta;
    m.m[1][2] = a.y * a.z * (1 - cosTheta) - a.x * sinTheta;
    m.m[1][3] = 0;

    m.m[2][0] = a.x * a.z * (1 - cosTheta) - a.y * sinTheta;
    m.m[2][1] = a.y * a.z * (1 - cosTheta) + a.x * sinTheta;
    m.m[2][2] = a.z * a.z + (1 - a.z * a.z) * cosTheta;
    m.m[2][3] = 0;
    return Transform(m, Transpose(m));
}

__host__ __device__ Transform LookAt(const Point3f& pos, const Point3f& look, const Vector3f& up)
{
    Matrix4x4 cameraToWorld;
    // Initialize fourth column of viewing matrix
    cameraToWorld.m[0][3] = pos.x;
    cameraToWorld.m[1][3] = pos.y;
    cameraToWorld.m[2][3] = pos.z;
    cameraToWorld.m[3][3] = 1;

    // Initialize first three columns of viewing matrix
    Vector3f dir = Normalize(look - pos);
    if (Cross(Normalize(up), dir).Length() == 0) 
        return Transform();

    Vector3f right = Normalize(Cross(Normalize(up), dir));
    Vector3f newUp = Cross(dir, right);
    cameraToWorld.m[0][0] = right.x;
    cameraToWorld.m[1][0] = right.y;
    cameraToWorld.m[2][0] = right.z;
    cameraToWorld.m[3][0] = 0.;
    cameraToWorld.m[0][1] = newUp.x;
    cameraToWorld.m[1][1] = newUp.y;
    cameraToWorld.m[2][1] = newUp.z;
    cameraToWorld.m[3][1] = 0.;
    cameraToWorld.m[0][2] = dir.x;
    cameraToWorld.m[1][2] = dir.y;
    cameraToWorld.m[2][2] = dir.z;
    cameraToWorld.m[3][2] = 0.;
    return Transform(Inverse(cameraToWorld), cameraToWorld);
}

__host__ __device__ Transform Transform::operator*(const Transform& t2) const
{
    return Transform(Matrix4x4::Mul(m, t2.m), Matrix4x4::Mul(t2.mInv, mInv));
}

__host__ __device__ bool Transform::SwapsHandedness() const
{
    float det = m.m[0][0] * (m.m[1][1] * m.m[2][2] - m.m[1][2] * m.m[2][1]) -
        m.m[0][1] * (m.m[1][0] * m.m[2][2] - m.m[1][2] * m.m[2][0]) +
        m.m[0][2] * (m.m[1][0] * m.m[2][1] - m.m[1][1] * m.m[2][0]);
    return det < 0;
}

__host__ __device__ Transform Orthographic(float zNear, float zFar)
{
    return Scale(1, 1, 1 / (zFar - zNear)) * Translate(Vector3f(0, 0, -zNear));
}

__host__ __device__ Transform Perspective(float fov, float n, float f)
{
    // Perform projective divide for perspective projection
    Matrix4x4 persp(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, f / (f - n), -f * n / (f - n),
        0, 0, 1, 0);

    // Scale canonical perspective view to specified field of view
    float invTanAng = 1 / std::tan(Radians(fov) / 2);
    return Scale(invTanAng, invTanAng, 1) * Transform(persp);
}