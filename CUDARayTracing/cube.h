#pragma once

#include "shape.h"

// Cube Declaration
class Cube : public Shape
{
public:
	// Cube Public Methods
	__device__ Cube() : material(nullptr) {}
	__device__ Cube(Transform transform, Material* material) : Shape(transform), material(material) {}

	__device__ ~Cube() { delete material; }
	// Return true if ray intersects Cube
	__device__ bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
	// Calculate sphere normal in point
	__device__ inline Normal3f NormalInPoint(const Point3f& point) const;
	// Returns AABB of Cube
	//Bounds3f BoundingBox() const;
private:
	// Cube Private Data
	Material* material;
};

__device__ inline bool Cube::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);

	// Intersect x plane
	float tx0 = -(ray.origin.x - 1) / ray.direction.x;
	float tx1 = -(ray.origin.x + 1) / ray.direction.x;
	if (tx0 > tx1) swap(tx0, tx1);

	// Intersect y plane
	float ty0 = -(ray.origin.y - 1) / ray.direction.y;
	float ty1 = -(ray.origin.y + 1) / ray.direction.y;
	if (ty0 > ty1) swap(ty0, ty1);

	// Intersect z plane
	float tz0 = -(ray.origin.z - 1) / ray.direction.z;
	float tz1 = -(ray.origin.z + 1) / ray.direction.z;
	if (tz0 > tz1) swap(tz0, tz1);

	// Calculate largest minimum and smallest maximum
	float tMin = fmax(tx0, fmax(ty0, tz0));
	float tMax = fmin(tx1, fmin(ty1, tz1));

	// If min > max there is no hit
	if (tMin > tMax) return false;

	// check first hit
	if (tMin > ShadowEpsilon && r.tMax > tMin)
	{
		r.tMax = tMin;
		isect->point = ObjectToWorld(ray(tMin));
		isect->normal = ObjectToWorld(NormalInPoint(ray(tMin)));
		isect->material = material;
		return true;
	}

	// check second hit
	if (tMax > ShadowEpsilon && r.tMax > tMax)
	{
		r.tMax = tMax;
		isect->point = ObjectToWorld(ray(tMax));
		isect->normal = ObjectToWorld(NormalInPoint(ray(tMax)));
		isect->material = material;
		return true;
	}
	return false;
}

__device__ inline Normal3f Cube::NormalInPoint(const Point3f& point) const
{
	// x axis plane
	if (point.x >  1 - ShadowEpsilon) return Normal3f( 1,  0,  0);
	if (point.x < -1 + ShadowEpsilon) return Normal3f(-1,  0,  0);
	// y axis plane
	if (point.y >  1 - ShadowEpsilon) return Normal3f( 0,  1,  0);
	if (point.y < -1 + ShadowEpsilon) return Normal3f( 0, -1,  0);
	// z axis plane
	if (point.z >  1 - ShadowEpsilon) return Normal3f( 0,  0,  1);
	if (point.z < -1 + ShadowEpsilon) return Normal3f( 0,  0, -1);
}