#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <vector>

template<typename DeviceVector>
class VectorIterator
{
public:
	using ValueType = typename DeviceVector::ValueType;
	using PointerType = ValueType*;
	using ReferenceType = ValueType&;
public:
	__host__ __device__  VectorIterator(PointerType ptr)
		: m_Ptr(ptr) {}

	__host__ __device__   VectorIterator& operator++()
	{
		m_Ptr++;
		return *this;
	}

	__host__ __device__  VectorIterator& operator++(int)
	{
		VectorIterator iterator = *this;
		++(*this);
		return iterator;
	}

	__host__ __device__  VectorIterator& operator--()
	{
		m_Ptr--;
		return *this;
	}

	__host__ __device__  VectorIterator& operator--(int)
	{
		VectorIterator iterator = *this;
		--(*this);
		return iterator;
	}

	__host__ __device__  ReferenceType operator[](int index)
	{
		return *(m_Ptr + index);
	}

	__host__ __device__  PointerType operator->()
	{
		return m_Ptr;
	}

	__host__ __device__  ReferenceType operator*()
	{
		return *m_Ptr;
	}

	__host__ __device__  bool operator==(const VectorIterator& other)const
	{
		return m_Ptr == other.m_Ptr;
	}

	__host__ __device__  bool operator!=(const VectorIterator& other)const
	{
		return !(*this == other);
	}

private:
	PointerType m_Ptr;

};

template<typename T>
class DeviceVector
{
public:
	using ValueType = T;
	using Iterator = VectorIterator<DeviceVector<T>>;
public:
	__host__ __device__  DeviceVector()
	{
		ReAlloc(2);
	}

	__host__ __device__  ~DeviceVector() 
	{ 
		Clear();
		::operator delete(m_Data, m_Capacity * sizeof(T));
	}

	__host__ __device__  void PushBack(const T& value)
	{
		if (m_Size >= m_Capacity)
			ReAlloc(m_Capacity + m_Capacity / 2);

		m_Data[m_Size] = value;
		m_Size++;
	}

	__host__ __device__  void PushBack(T&& value)
	{
		if (m_Size >= m_Capacity)
			ReAlloc(m_Capacity + m_Capacity / 2);

		m_Data[m_Size] = std::move(value);
		m_Size++;
	}

	__host__ __device__  void PopBack()
	{
		if (m_Size > 0)
		{
			m_Size--;
			m_Data[m_Size].~T();
		}
	}

	__host__ __device__  void Clear()
	{
		for (size_t i = 0; i < m_Size; i++)
			m_Data[i].~T();

		m_Size = 0;
	}

	__host__ __device__  T& operator[](size_t index) { return m_Data[index]; }
	__host__ __device__  const T& operator[](size_t index) const { return m_Data[index]; }

	__host__ __device__  size_t Size() const { return m_Size; }

	__host__ __device__  Iterator begin()
	{
		return Iterator(m_Data);
	}

	__host__ __device__  Iterator end()
	{
		return Iterator(m_Data + m_Size);
	}

private:
	__host__ __device__  void ReAlloc(size_t newCapacity)
	{
		T* newBlock = (T*)::operator new(newCapacity * sizeof(T));

		if (newCapacity < m_Size)
			m_Size = newCapacity;

		for (size_t i = 0; i < m_Size; i++)
			new (&newBlock[i]) T(std::move(m_Data[i]));

		for (size_t i = 0; i < m_Size; i++)
			m_Data[i].~T();

		::operator delete(m_Data, m_Capacity * sizeof(T));
		m_Data = newBlock;
		m_Capacity = newCapacity;
	}

private:
	T* m_Data = nullptr;

	size_t m_Size = 0;
	size_t m_Capacity = 0;
};
