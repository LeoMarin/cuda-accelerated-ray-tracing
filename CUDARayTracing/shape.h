#pragma once

#include "transform.h"
#include "interaction.h"
#include "material.h"

class Shape
{
public:
	// Shape Intefrace
	__device__ Shape() {}
	__device__ Shape(Transform ObjectToWorld) : ObjectToWorld(ObjectToWorld), WorldToObject(Transform(ObjectToWorld.GetInverseMatrix())) {}
	__device__ virtual ~Shape() {}
	// Return true if ray intersects shape
	__device__ virtual bool Intersect(const Ray& ray, SurfaceInteraction *isect) const = 0;
	// Returns AABB of shape
	//virtual Bounds3f BoundingBox() const = 0;
protected:
	// Shape public data
	const Transform ObjectToWorld, WorldToObject;
};

// Sphere Declaration
class Sphere : public Shape
{
public:
	// Sphere Public Methods
	__device__ Sphere() : radius(0), material(nullptr) {}
	__device__ Sphere(float r, Transform transform, Material* material) : Shape(transform), radius(r), material(material){}
	//Sphere(Vector3f origin, float radius, Material* material);

	__device__ ~Sphere() { delete material; }
	// Return true if ray intersects sphere
	__device__ bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
	// Calculate sphere normal in point
	__device__ inline Normal3f NormalInPoint(const Point3f& point) const;
	// Returns AABB of sphere
	//Bounds3f BoundingBox() const;
private:
	// Sphere Private Data
	float radius;
	Material* material;
};

__device__ inline bool Sphere::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);
	// transform origin point to vector
	Vector3f originVector(ray.origin);

	// calculate a, b and c factors of quadratic equation
	float a = ray.direction.LengthSquared();
	float b = 2 * Dot(originVector, ray.direction);
	float c = originVector.LengthSquared() - radius * radius;

	// find roots of the quadratic equation
	float t0, t1;
	if (!Quadratic(a, b, c, &t0, &t1))
		return false; // no real solutions means no hit

					  // check first hit
	if (t0 > ShadowEpsilon && ray.tMax > t0)
	{
		r.tMax = t0;
		isect->point = ObjectToWorld(ray(t0));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t0)));
		isect->material = material;
		return true;
	}

	// check second hit
	if (t1 > ShadowEpsilon && ray.tMax > t1)
	{
		r.tMax = t1;
		isect->point = ObjectToWorld(ray(t1));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t1)));
		isect->material = material;
		return true;
	}
	return false;
}

__device__ inline Normal3f Sphere::NormalInPoint(const Point3f& point) const
{
	// v = p - center
	// vector v has direction of sphere normal in point p
	return Normal3f(Vector3f(point) / radius);
}

