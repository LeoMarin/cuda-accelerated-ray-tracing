#pragma once

#include <cmath>

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

class Vector3f;
class Point3f;
class Normal3f;

static constexpr float ShadowEpsilon = 0.0001f;
static constexpr float MachineEpsilon = 0.0000001f;
static constexpr float Pi =         3.14159265f;
static constexpr float InvPi =      0.31830988f;
static constexpr float Inv2Pi =     0.15915494f;
static constexpr float Inv4Pi =     0.07957747f;
static constexpr float PiOver2 =    1.57079632f;
static constexpr float PiOver4 =    0.78539816f;
static constexpr float Sqrt2 =      1.41421356f;

__host__ __device__ inline float gamma(int n) { return (n * MachineEpsilon) / (1 - n * MachineEpsilon); }
__host__ __device__ inline float Radians(float deg) { return (Pi / 180) * deg; }
__host__ __device__ inline float Degrees(float rad) { return (180 * InvPi) * rad; }

template<typename T>
__host__ __device__ inline void swap(T& a, T& b) { T temp = a; a = b; b = temp; }

// find real roots of quadratic equation
// return false if no real solutions
__device__ inline bool Quadratic(float a, float b, float c, float* t0, float* t1)
{
    float discriminatn = b * b - 4 * a * c;

    // no real solutions
    if (discriminatn < 0.f) 
        return false;

    float rootDiscriminant = sqrt(discriminatn);
    float aInv = 0.5f / a;

    *t0 = (-b - rootDiscriminant) * aInv;
    *t1 = (-b + rootDiscriminant) * aInv;

    // t0 should always be smaller
    if (*t0 > *t1) swap(*t0, *t1);
    return true;
}

/*
/
/ *** Vector3f ***
/
*/
class Vector3f
{
public:
    // Vector3f public methods
    __host__ __device__ Vector3f() : x(0), y(0), z(0) {}
    __host__ __device__ Vector3f(float f) : x(f), y(f), z(f) {}
    __host__ __device__ Vector3f(float x, float y, float z) : x(x), y(y), z(z) {}
    __host__ __device__ Vector3f(const Vector3f& other) : x(other.x), y(other.y), z(other.z) {}

    __host__ __device__ explicit Vector3f(const Point3f& p);
    __host__ __device__ explicit Vector3f(const Normal3f& n);
    __host__ __device__ ~Vector3f() {}

    __host__ __device__ inline float Length() const { return sqrtf(x * x + y * y + z * z); }
    __host__ __device__ inline float LengthSquared() const { return x * x + y * y + z * z; }

    __host__ __device__ inline const Vector3f& operator+() const { return *this; }
    __host__ __device__ inline Vector3f operator-() const { return Vector3f(-x, -y, -z); }
    __host__ __device__ inline float operator[](int i) const { return (i == 0) ? x : ((i == 1) ? y : z); }
    __host__ __device__ inline float& operator[](int i) { return (i == 0) ? x : ((i == 1) ? y : z); };

    __host__ __device__ inline Vector3f& operator+=(const Vector3f &v2);
    __host__ __device__ inline Vector3f& operator-=(const Vector3f &v2);
    __host__ __device__ inline Vector3f& operator*=(const Vector3f &v2);
    __host__ __device__ inline Vector3f& operator/=(const Vector3f &v2);
    __host__ __device__ inline Vector3f& operator*=(const float t);
    __host__ __device__ inline Vector3f& operator/=(const float t);

    __host__ __device__ bool operator==(const Vector3f& n) const { return x == n.x && y == n.y && z == n.z; }
    __host__ __device__ bool operator!=(const Vector3f &n) const { return x != n.x || y != n.y || z != n.z; }

public:
    // Vector3f public data
    float x, y, z;
};

// iostream operator overloading
inline std::istream& operator>>(std::istream &is, Vector3f &t) {
    is >> t.x >> t.y >> t.z;
    return is;
}

inline std::ostream& operator<<(std::ostream &os, const Vector3f &t) {
    os << t.x << " " << t.y << " " << t.z;
    return os;
}

// implementation of Vector3f public methods

__host__ __device__ inline Vector3f operator+(const Vector3f& v1, const Vector3f& v2)
{
    return Vector3f(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

__host__ __device__ inline Vector3f operator-(const Vector3f& v1, const Vector3f& v2)
{
    return Vector3f(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

__host__ __device__ inline Vector3f operator*(const Vector3f& v1, const Vector3f& v2)
{
    return Vector3f(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

__host__ __device__ inline Vector3f operator/(const Vector3f& v1, const Vector3f& v2)
{
    return Vector3f(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
}

__host__ __device__ inline Vector3f operator*(float t, const Vector3f& v)
{
    return Vector3f(t*v.x, t*v.y, t*v.z);
}

__host__ __device__ inline Vector3f operator/(Vector3f v, float t)
{
    return Vector3f(v.x/t, v.y/t, v.z/t);
}

__host__ __device__ inline Vector3f operator*(const Vector3f &v, float t)
{
    return Vector3f(t*v.x, t*v.y, t*v.z);
}

// operator overloading
__host__ __device__ inline Vector3f& Vector3f::operator+=(const Vector3f& v)
{
    x  += v.x;
    y  += v.y;
    z  += v.z;
    return *this;
}

__host__ __device__ inline Vector3f& Vector3f::operator*=(const Vector3f& v)
{
    x  *= v.x;
    y  *= v.y;
    z  *= v.z;
    return *this;
}

__host__ __device__ inline Vector3f& Vector3f::operator/=(const Vector3f& v)
{
    x  /= v.x;
    y  /= v.y;
    z  /= v.z;
    return *this;
}

__host__ __device__ inline Vector3f& Vector3f::operator-=(const Vector3f& v)
{
    x  -= v.x;
    y  -= v.y;
    z  -= v.z;
    return *this;
}

__host__ __device__ inline Vector3f& Vector3f::operator*=(const float t)
{
    x  *= t;
    y  *= t;
    z  *= t;
    return *this;
}

__host__ __device__ inline Vector3f& Vector3f::operator/=(const float t)
{
    float k = 1.0f / t;

    x  *= k;
    y  *= k;
    z  *= k;
    return *this;
}

__host__ __device__ inline Vector3f UnitVector(Vector3f v)
{
    return v / v.Length();
}

/*
/
/ *** Point3f ***
/
*/

class Point3f
{

public:
    // Point3f Public Methods
    __host__ __device__ Point3f() { x = y = z = 0; }
    __host__ __device__ Point3f(float x, float y, float z) : x(x), y(y), z(z) {}
    __host__ __device__ explicit Point3f(const Vector3f& v) : x(v.x), y(v.y), z(v.z) {}

    __host__ __device__ explicit operator Vector3f() const { return Vector3f(x, y, z); } // ??

    __host__ __device__ Point3f operator+(const Point3f& p) const { return Point3f(x + p.x, y + p.y, z + p.z); }
    __host__ __device__ Point3f operator+(const Vector3f& v) const { return Point3f(x + v.x, y + v.y, z + v.z); }
    __host__ __device__ Vector3f operator-(const Point3f& p) const { return Vector3f(x - p.x, y - p.y, z - p.z); }
    __host__ __device__ Point3f operator-(const Vector3f& v) const { return Point3f(x - v.x, y - v.y, z - v.z); }
    __host__ __device__ Point3f operator*(float f) const { return Point3f(f * x, f * y, f * z); }
    __host__ __device__ Point3f operator/(float f) const { float inv = (float)1 / f; return Point3f(inv * x, inv * y, inv * z); }

    __host__ __device__ inline float operator[](int i) const { return (i == 0) ? x : ((i == 1) ? y : z); }
    __host__ __device__ inline float& operator[](int i) { return (i == 0) ? x : ((i == 1) ? y : z); };
    __host__ __device__ Point3f operator-() const { return Point3f(-x, -y, -z); }

    __host__ __device__ Point3f& operator+=(const Vector3f& v);
    __host__ __device__ Point3f& operator-=(const Vector3f& v);
    __host__ __device__ Point3f& operator+=(const Point3f& p);
    __host__ __device__ Point3f& operator*=(float f);
    __host__ __device__ Point3f& operator/=(float f);

    __host__ __device__ bool operator==(const Point3f &p) const { return x == p.x && y == p.y && z == p.z; }
    __host__ __device__ bool operator!=(const Point3f &p) const { return x != p.x || y != p.y || z != p.z; }

    // Point3f Public Data
    float x, y, z;
};

__host__ __device__ inline Point3f& Point3f::operator+=(const Vector3f& v)
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}

__host__ __device__ inline Point3f& Point3f::operator-=(const Vector3f& v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}

__host__ __device__ inline Point3f& Point3f::operator+=(const Point3f& p)
{
    x += p.x;
    y += p.y;
    z += p.z;
    return *this;
}

__host__ __device__ inline Point3f& Point3f::operator*=(float f)
{
    x *= f;
    y *= f;
    z *= f;
    return *this;
}

__host__ __device__ inline Point3f& Point3f::operator/=(float f)
{
    float inv = (float)1 / f;
    x *= inv;
    y *= inv;
    z *= inv;
    return *this;
}

/*
/
/ *** Normal3f ***
/
*/

class Normal3f
{

public:
    // Normal3 Public Methods
    __host__ __device__ Normal3f() { x = y = z = 0; }
    __host__ __device__ Normal3f(float x, float y, float z) : x(x), y(y), z(z) {}
    __host__ __device__ explicit Normal3f(const Vector3f &v) : x(v.x), y(v.y), z(v.z) {}

    __host__ __device__ float LengthSquared() const { return x * x + y * y + z * z; }
    __host__ __device__ float Length() const { return std::sqrtf(LengthSquared()); }

    __host__ __device__ Normal3f operator+(const Normal3f &n) const { return Normal3f(x + n.x, y + n.y, z + n.z); }
    __host__ __device__ Normal3f operator-(const Normal3f &n) const { return Normal3f(x - n.x, y - n.y, z - n.z); }
    __host__ __device__ Normal3f operator*(float f) const { return Normal3f(f * x, f * y, f * z); }
    __host__ __device__ Normal3f operator/(float f) const { float inv = (float)1 / f; return Normal3f(x * inv, y * inv, z * inv); }

    __host__ __device__ inline float operator[](int i) const { return (i == 0) ? x : ((i == 1) ? y : z); }
    __host__ __device__ inline float& operator[](int i) { return (i == 0) ? x : ((i == 1) ? y : z); };
    __host__ __device__ Normal3f operator-() const { return Normal3f(-x, -y, -z); }

    __host__ __device__ Normal3f& operator+=(const Normal3f& n);
    __host__ __device__ Normal3f& operator-=(const Normal3f& n);
    __host__ __device__ Normal3f& operator*=(float f);
    __host__ __device__ Normal3f& operator/=(float f);

    __host__ __device__ bool operator==(const Normal3f& n) const { return x == n.x && y == n.y && z == n.z; }
    __host__ __device__ bool operator!=(const Normal3f &n) const { return x != n.x || y != n.y || z != n.z; }

    // Normal3 Public Data
    float x, y, z;
};

__host__ __device__ inline Normal3f& Normal3f::operator+=(const Normal3f& n)
{
    x += n.x;
    y += n.y;
    z += n.z;
    return *this;
}

__host__ __device__ inline Normal3f& Normal3f::operator-=(const Normal3f& n)
{
    x -= n.x;
    y -= n.y;
    z -= n.z;
    return *this;
}

__host__ __device__ inline Normal3f& Normal3f::operator*=(float f)
{
    x *= f;
    y *= f;
    z *= f;
    return *this;
}

__host__ __device__ inline Normal3f& Normal3f::operator/=(float f)
{
    float inv = (float)1 / f;
    x *= inv;
    y *= inv;
    z *= inv;
    return *this;
}

/*
/
/ *** Ray ***
/
*/

class Ray
{
public:
    // Ray public methods
    __device__ Ray() :origin(Point3f()), direction(Vector3f()), tMax(INFINITY) {}
    __device__ Ray(const Point3f& origin, const Vector3f& direction, float tMax = INFINITY) : origin(origin), direction(direction), tMax(tMax) {}

    __device__ ~Ray() {}

    // gets point on ray p(t) = origin + t * direction
    __device__ inline Point3f operator()(float t) const { return origin + t * direction; }

public:
    // Ray public data
    Point3f origin;
    Vector3f direction;
    mutable float tMax;
};

/*
/
/ *** Inline functions ***
/
*/

__host__ __device__ inline Vector3f::Vector3f(const Point3f& p)
    : x(p.x), y(p.y), z(p.z) 
{}

__host__ __device__ inline Vector3f::Vector3f(const Normal3f& p)
    : x(p.x), y(p.y), z(p.z) 
{}

// Abs
__host__ __device__ inline Vector3f Abs(const Vector3f& v)
{
    return Vector3f(std::abs(v.x), std::abs(v.y), std::abs(v.z));
}

__host__ __device__ inline Point3f Abs(const Point3f& p)
{
    return Point3f(std::abs(p.x), std::abs(p.y), std::abs(p.z));
}

__host__ __device__ inline Normal3f Abs(const Normal3f& n)
{
    return Normal3f(std::abs(n.x), std::abs(n.y), std::abs(n.z));
}

// Dot
__host__ __device__ inline float Dot(const Vector3f& v1, const Vector3f& v2)
{
    return v1.x *v2.x + v1.y *v2.y  + v1.z *v2.z;
}

__host__ __device__ inline float Dot(const Vector3f& v1, const Normal3f& n2)
{
    return v1.x *n2.x + v1.y *n2.y  + v1.z *n2.z;
}

__host__ __device__ inline float Dot(const Normal3f& n1, const Vector3f& v2)
{
    return n1.x *v2.x + n1.y *v2.y  + n1.z *v2.z;
}

__host__ __device__ inline float Dot(const Normal3f& n1, const Normal3f& n2)
{
    return n1.x *n2.x + n1.y *n2.y  + n1.z *n2.z;
}

// Abs Dot
__host__ __device__ inline float AbsDot(const Vector3f& v1, const Vector3f& v2)
{
    return std::abs(v1.x *v2.x + v1.y *v2.y  + v1.z *v2.z);
}

__host__ __device__ inline float AbsDot(const Vector3f& v1, const Normal3f& n2)
{
    return std::abs(v1.x *n2.x + v1.y *n2.y  + v1.z *n2.z);
}

__host__ __device__ inline float AbsDot(const Normal3f& n1, const Vector3f& v2)
{
    return std::abs(n1.x *v2.x + n1.y *v2.y  + n1.z *v2.z);
}

__host__ __device__ inline float AbsDot(const Normal3f& n1, const Normal3f& n2)
{
    return std::abs(n1.x *n2.x + n1.y *n2.y  + n1.z *n2.z);
}

// Cross
__host__ __device__ inline Vector3f Cross(const Vector3f& v1, const Vector3f& v2)
{
    return Vector3f( (v1.y*v2.z - v1.z*v2.y),
        (-(v1.x*v2.z - v1.z*v2.x)),
        (v1.x*v2.y - v1.y*v2.x));
}

__host__ __device__ inline Vector3f Cross(const Vector3f& v1, const Normal3f& n2)
{
    return Vector3f( (v1.y*n2.z - v1.z*n2.y),
        (-(v1.x*n2.z - v1.z*n2.x)),
        (v1.x*n2.y - v1.y*n2.x));
}

__host__ __device__ inline Vector3f Cross(const Normal3f& n1, const Vector3f& v2)
{
    return Vector3f( (n1.y*v2.z - n1.z*v2.y),
        (-(n1.x*v2.z - n1.z*v2.x)),
        (n1.x*v2.y - n1.y*v2.x));
}

// Normalize
__host__ __device__ inline Vector3f Normalize(const Vector3f& v)
{
    return v / v.Length();
}

__host__ __device__ inline Normal3f Normalize(const Normal3f& n)
{
    return n / n.Length();
}

// Distance
__host__ __device__ inline float Distance(const Point3f& p1, const Point3f& p2)
{
    return (p1 - p2).Length();
}

__host__ __device__ inline float DistanceSquared(const Point3f& p1, const Point3f& p2)
{
    return (p1 - p2).LengthSquared();
}

// Reflect
__device__ inline Vector3f Reflect(const Vector3f& vector, const Normal3f& normal)
{
    // calculate reflection ray around given
    return vector - Vector3f(normal * 2 * Dot(vector, normal));
}

//// Min and max
//__host__ __device__ float MinComponent(const Vector3f& v)
//{
//    return std::min(v.x, std::min(v.y, v.z));
//}
//
//__host__ __device__ float MaxComponent(const Vector3f& v)
//{
//    return std::max(v.x, std::max(v.y, v.z));
//}
//
//__host__ __device__ int MaxDimension(const Vector3f& v)
//{
//    return (v.x > v.y) ? ((v.x > v.z) ? 0 : 2) : ((v.y > v.z) ? 1 : 2);
//}
//
//__host__ __device__ Vector3f Min(const Vector3f& p1, const Vector3f& p2)
//{
//    return Vector3f(std::min(p1.x, p2.x), std::min(p1.y, p2.y),
//        std::min(p1.z, p2.z));
//}
//
//__host__ __device__ Vector3f Max(const Vector3f& p1, const Vector3f& p2)
//{
//    return Vector3f(std::max(p1.x, p2.x), std::max(p1.y, p2.y),
//        std::max(p1.z, p2.z));
//}
//
//__host__ __device__ Point3f Min(const Point3f& p1, const Point3f& p2)
//{
//    return Point3f(std::min(p1.x, p2.x), std::min(p1.y, p2.y),
//        std::min(p1.z, p2.z));
//}
//
//__host__ __device__ Point3f Max(const Point3f& p1, const Point3f& p2)
//{
//    return Point3f(std::max(p1.x, p2.x), std::max(p1.y, p2.y),
//        std::max(p1.z, p2.z));
//}
//
//// Floor and Ceil
//__host__ __device__ Point3f Floor(const Point3f& p)
//{
//    return Point3f(std::floor(p.x), std::floor(p.y), std::floor(p.z));
//}
//
//__host__ __device__ Point3f Ceil(const Point3f& p)
//{
//    return Point3f(std::ceil(p.x), std::ceil(p.y), std::ceil(p.z));
//}
