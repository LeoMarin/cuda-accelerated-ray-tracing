﻿#include <iostream>
#include <fstream>

#include <math.h>
#include <time.h>

#include <curand_kernel.h>

#include "camera.h"
#include "scene.h"

// check error return code
#define checkCudaErrors(val) check_cuda( (val), #val, __FILE__, __LINE__ )
void check_cuda(cudaError_t result, char const *const func, const char *const file, int const line) 
{
    if (result) {
        std::cerr << "CUDA error = " << static_cast<unsigned int>(result) << " at " <<
            file << ":" << line << " '" << func << "' \n";
        // Make sure we call CUDA Device Reset before exiting
        cudaDeviceReset();
        exit(99);
    }
}

__device__ Vector3f Color(const Ray& ray, Scene** d_scene, curandState* local_rand_state) 
{
    Ray currentRay = ray;
    Vector3f currentAttenuation  = Vector3f(1);
    // GPU must use iteration over recursion
    for (int i = 0; i < 100; i++)
    {
        // Check for current ray hit
        SurfaceInteraction isect;
        if ((*d_scene)->Intersect(currentRay, &isect))
        {
            Ray scatterRay;
            Vector3f albedo = isect.material->Albedo();
            // check if ray will scatter
            if (isect.material->Scatter(currentRay, &isect, scatterRay, local_rand_state))
            {
                currentAttenuation *= albedo;
                currentRay = scatterRay;
            }
            else
            {
                return Vector3f(); // no scattering
            }
        }
        // no hit
        else
        {
            // get value of y and clamp it to 0 < t < 1
            float t = 0.5f * (UnitVector(currentRay.direction).y + 1.f);
            // calculate color using linear interpolation (lerp)
            // blendedValue = (1-t)*startValue + t*endValue
            Vector3f color = Vector3f(1, 1, 1) * (1 - t) + Vector3f(0.5f, 0.7f, 1.0f) * t;
            return color * currentAttenuation;
        }
    }
    return Vector3f(); // exceded iteration
}

__global__ void Render(Vector3f *fb, Camera** d_camera, Scene** d_scene, curandState *rand_state) 
{
    // calculate current pixel index
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;

    int nx = (*d_camera)->width;
    int ny = (*d_camera)->height;
    if(i >= nx || j >= ny) return;

    int pixel_index = j * nx + i;
    curandState local_rand_state = rand_state[pixel_index];

    // number of ray casts per pixel
    int numberOfSamples = 1000;

    Vector3f color;

    // cast multiple rays for single pixel
    for (size_t s = 0; s < numberOfSamples; s++)
    {
        // calculate position of current ray with offset
        float u = (float)(i + curand_uniform(&local_rand_state)) / nx;
        float v = (float)(j + curand_uniform(&local_rand_state)) / ny;

        // get color of current pixel using ray tracing
        color += Color((*d_camera)->GetRay(u, v), d_scene, &local_rand_state);
    }

    // get average color of current pixel
    color = color / numberOfSamples;

    // gama corelation
    color = Vector3f(sqrt(color.x), sqrt(color.y), sqrt(color.z));

    fb[pixel_index] = color;
}

__global__ void render_init(int max_x, int max_y, curandState* rand_state)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    if((i >= max_x) || (j >= max_y)) return;
    int pixel_index = j*max_x + i;
    //Each thread gets same seed, a different sequence number, no offset
    curand_init(1984, pixel_index, 0, &rand_state[pixel_index]);
}

__global__ void CreateWorld(Camera** d_camera, int nx, int ny, Scene** d_scene, MeshVertexData* mesh)
{
    if (threadIdx.x == 0 && blockIdx.x == 0) 
    {
        // Setup camera
        Vector3f lookFrom = Vector3f(0, 8, 15);
        Vector3f lookAt = Vector3f(0, 5, 0);
        *d_camera = new Camera(lookFrom, lookAt, nx, ny);

        // Setup scene
        *d_scene = new Scene(mesh);
         
    }
}

__global__ void FreeWorld(Camera** d_camera, Scene** d_scene)
{
    delete* d_camera;
    //delete* d_scene;
}


void WriteImage(Vector3f* fb, int nx, int ny)
{
    // Output FB as Image
    std::cout << "Writing image to file..." << std::endl;
    // open .ppm file
    std::ofstream ofs;
    ofs.open("./GPUimages/out.ppm");
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    for (int j = ny-1; j >= 0; j--) 
    {
        for (int i = 0; i < nx; i++) 
        {
            size_t pixel_index = j*nx + i;
            // scale color to 0-255 for rgb values
            int ir = int(255.99*fb[pixel_index].x);
            int ig = int(255.99*fb[pixel_index].y);
            int ib = int(255.99*fb[pixel_index].z);
            // write color to output file
            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }

    // close .ppm file
    ofs.close();
}

int main() 
{
    int nx = 300;
    int ny = 200;
    int tx = 8;
    int ty = 8;

    int num_pixels = nx*ny;

    std::cerr << "Setting up scene..\n";

    // allocate FB
    size_t fb_size = num_pixels*sizeof(Vector3f);
    Vector3f* fb;
    checkCudaErrors(cudaMallocManaged((void **)&fb, fb_size));

    // allocate random state
    curandState *d_rand_state;
    checkCudaErrors(cudaMalloc((void **)&d_rand_state, num_pixels*sizeof(curandState)));

    // allocate Camera
    Camera** d_camera;
    checkCudaErrors(cudaMalloc((void**)&d_camera, sizeof(Camera*)));

    // read .obj data
    MeshVertexData* mesh = new MeshVertexData("obj/golem.obj");

    // allocate scene
    Scene** d_scene;
    checkCudaErrors(cudaMalloc((void**)&d_scene, sizeof(Scene*)));

    // setup world objects and camera
    CreateWorld<<<1,1>>>(d_camera, nx, ny, d_scene, mesh);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());

    std::cerr << "Rendering a " << nx << "x" << ny << " image ";
    std::cerr << "in " << tx << "x" << ty << " blocks.\n";

    // start timing
    clock_t start, stop;
    start = clock();

    // Render our buffer
    dim3 blocks(nx/tx+1,ny/ty+1);
    dim3 threads(tx,ty);

    // Initialize renderer
    render_init<<<blocks, threads>>>(nx, ny, d_rand_state);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());

    // Calculate values of FrameBuffer
    Render<<<blocks, threads>>>(fb, d_camera, d_scene, d_rand_state);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize()); // wait for GPU to finish

    // stop timing
    stop = clock();
    double timer_seconds = ((double)(stop - start)) / CLOCKS_PER_SEC;
    std::cerr << "took " << timer_seconds << " seconds.\n";

    // Output FB as Image
    WriteImage(fb, nx, ny);

    // cleanup
    checkCudaErrors(cudaDeviceSynchronize());
    FreeWorld<<<1,1>>>(d_camera, d_scene);
    checkCudaErrors(cudaGetLastError());

    checkCudaErrors(cudaFree(d_camera));
    checkCudaErrors(cudaFree(d_scene));
    checkCudaErrors(cudaFree(fb));
}