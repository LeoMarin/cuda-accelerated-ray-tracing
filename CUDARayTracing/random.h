#pragma once

#include <curand_kernel.h>
#include "geometry.h"

__device__ inline float RandomFloat(curandState* local_rand_state)
{
	return curand_uniform(local_rand_state);
}

__device__ inline Vector3f RandomPointInUnitSphere(curandState* local_rand_state)
{
	Vector3f p;
	do
	{
		p = Vector3f(RandomFloat(local_rand_state), RandomFloat(local_rand_state), RandomFloat(local_rand_state));
	} while (p.LengthSquared() >= 1);
	return p;
}

__device__ inline Vector3f RandomPointInUnitDisk(curandState* local_rand_state)
{
	Vector3f point;
	do
	{
		point = Vector3f(RandomFloat(local_rand_state), RandomFloat(local_rand_state), 0);
	} while (Dot(point, point) >= 1);
	return point;
}