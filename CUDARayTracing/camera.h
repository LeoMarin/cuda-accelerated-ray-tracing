#pragma once

#include "geometry.h"

// Camera Declaration
class Camera
{
public:
	// constructors
	__host__ __device__ Camera(const Vector3f& lookFrom, const Vector3f& lookAt, int nx, int ny);

	// destructor
	__host__ __device__ ~Camera() {}

	__device__ Ray GetRay(float s, float t) const;

	// camera public data
	// image dimensions
	int width, height;
private:
	// camera defined in center of coordinate system
	Vector3f origin;
	// bottom left corner of image
	Vector3f bottomLeftCorner;

	// horizontal and vertical vector defining image
	Vector3f horizontal;
	Vector3f vertical;

	// local coordinates
	Vector3f u, v, w;

}; 

// Camera Constructor Definition
__host__ __device__ inline Camera::Camera(const Vector3f& lookFrom, const Vector3f& lookAt, int nx, int ny)
	:width(nx), height(ny), origin(lookFrom)
{
	// calculate local coordinates
	w = UnitVector(lookFrom - lookAt);
	u = Cross(Vector3f(0,1,0), w);
	v = Cross(w, u);

	// fov in rad
	float theta = 40 * 3.14159f / 180;
	float halfHeight = tan(theta / 2);
	float halfWidth = ((float)nx / ny) *halfHeight;

	//m_BottomLeftCorner = Vec3f(-halfWidth, -halfHeight, -1);
	bottomLeftCorner = origin - u * halfWidth - v * halfHeight - w;

	// calculate image vectors
	horizontal = u * 2 * halfWidth;
	vertical = v * 2 * halfHeight;

}

__device__ inline Ray Camera::GetRay(float s, float t) const
{
	// transform pixel to camera space
	//s /= width;
	//t /= height;

	return Ray(Point3f(origin), bottomLeftCorner + horizontal * s + vertical * t - origin);
}
